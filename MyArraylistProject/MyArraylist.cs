﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArraylistProject
{
    class MyArraylist<E>
    {
        private static readonly int DEFAULT_CAPACITY = 5;

        private int Capacity = DEFAULT_CAPACITY;

        private int Size = 0;

        private Object[] items;

        public MyArraylist()
        {
            items = new Object[DEFAULT_CAPACITY];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Numarul de elemente din array</returns>
        public int getSize()
        {
            return this.Size;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Capacitatea maxima pentru array</returns>
        public int getCapacity()
        {
            return Capacity;
        }

        /// <summary>
        /// Dubleaza capacitatea in cazul in care s-a depasit Size-ul
        /// </summary>
        private void doubleSize()
        {
            int NewCapacity = Capacity * 2;

            Object[] temp = items;

            items = new Object[NewCapacity];

            Array.Copy(temp, items, Capacity);

            Capacity = NewCapacity;
        }

        /// <summary>
        /// Adaugam un nou element in array
        /// </summary>
        /// <param name="item">Elementul pe care vrem sa-l adaugam in array</param>
        public void addItem(E item)
        {
            if (Size == Capacity)
            {
                doubleSize();
            }

            items[Size++] = item;
        }

        /// <summary>
        /// In cazul in care index ul dat este negativ sau mai mare ca size ul curent se va arunca o eroare
        /// </summary>
        /// <param name="index">Pozitia elementului cautat</param>
        /// <returns>Elementul de pe pozitia index</returns>
        public E getItem(int index)
        {
            if (index >= Size || index < 0) {
                throw new IndexOutOfRangeException("Index: " + index + ", Size: " + Size);
            }

            return (E)items[index];
        }

        /// <summary>
        /// In cazul in care index ul dat este negativ sau mai mare ca size ul curent se va arunca o eroare
        /// </summary>
        /// <param name="index">Pozitia elementului pe care vrem sa l stergem</param>
        /// <returns>Elementul sters</returns>
        public E removeItem(int index)
        {
            if (index >= Size || index < 0)
            {
                throw new IndexOutOfRangeException("Index: " + index + ", Size: " + Size);
            }

            E removedItem = (E)items[index];

            for (int i = index; i < Size; i++)
            {
                items[i] = items[i + 1];
            }

            Size--;

            return removedItem;
        }

        /// <summary>
        /// Sterge toate elementele din array cu valoarea = value
        /// </summary>
        /// <param name="value">Valoarea pe care vrem sa o stergem din array</param>
        public void removeValue(E value)
        {
            for (int i = 0; i < Size; i++)
            {
                if (value.Equals(items[i]))
                {
                    for (int j = i; j < Size; j++)
                    {
                        items[j] = items[j + 1];
                    }

                    Size--;
                }
            }
        }

        /// <summary>
        /// Afiseaza toate elementele din array
        /// </summary>
        public void listAllItems()
        {
            Console.Write("Items: ");

            for (int i = 0; i < Size; i++)
            {
                Console.Write(items[i] + " ");
            }
        }
    }
}
