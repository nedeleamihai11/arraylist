﻿using System;

namespace MyArraylistProject
{
    class Program
    {
        public static void Main(string[] args)
        {
            MyArraylist<int> list = new MyArraylist<int>();

            list.addItem(2);
            list.addItem(4);
            list.addItem(6);
            list.addItem(8);
            list.addItem(10);

            list.listAllItems();

            Console.WriteLine();

            Console.WriteLine("Size: " + list.getSize() + " Capacity: " + list.getCapacity());

            list.addItem(12);

            list.addItem(14);

            list.addItem(16);

            list.addItem(6);

            list.listAllItems();

            Console.WriteLine();

            Console.WriteLine("Size: " + list.getSize() + " Capacity: " + list.getCapacity());

            Console.WriteLine("Item: " + list.getItem(5));

            Console.WriteLine("Removed Item: " + list.removeItem(1));

            list.listAllItems();

            Console.WriteLine();

            Console.WriteLine("Size: " + list.getSize() + " Capacity: " + list.getCapacity());

            list.removeValue(6);

            list.listAllItems();

            Console.WriteLine();

            Console.WriteLine("Size: " + list.getSize() + " Capacity: " + list.getCapacity());

            Console.WriteLine("Item: " + list.getItem(6));
        }
    }
}
